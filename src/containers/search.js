import { connect } from 'react-redux';

import search from '../components/search';
import { setNameFilter } from '../actions/actions';

const mapStateToProps = state => ({
  filter: state.nameFilter,
});

const mapDispatchToProps = dispatch => ({
  onChange: ({ target }) => {
    const { value } = target;
    dispatch(setNameFilter(value));
  },
});

const Search = connect(
  mapStateToProps,
  mapDispatchToProps
)(search);

export default Search;
