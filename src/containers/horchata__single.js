/* eslint camelcase: 0 */
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Single from '../components/single';
import { apiFetchIfNeeded } from '../actions/actions';

const mapStateToProps = (state, ownProps) => {
  const {
    history,
    match: {
      params: { id },
    },
  } = ownProps;

  const { apiCalls } = state;
  const singleCall = `horchata/${id}`;

  const { [singleCall]: single } = apiCalls;
  if (!single) return { isFetching: true };

  const { isFetching, items } = single;
  const { detail } = items;
  if (detail === 'Not found.') {
    console.log('404');
    history.push('/404');
    return { error: true };
  }

  return { isFetching, item: items };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const {
    match: {
      params: { id },
    },
  } = ownProps;
  dispatch(apiFetchIfNeeded(`horchata/${id}`));
  return {};
};

const HosrchataSingle = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Single)
);

export default HosrchataSingle;
