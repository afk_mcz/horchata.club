import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';

import Router from './router';
import horchataApp from '../../reducers';

import './style.css';

const store = createStore(
  horchataApp,
  applyMiddleware(logger, thunkMiddleware)
);

export default () => (
  <Provider store={store}>
    <Router />
  </Provider>
);
