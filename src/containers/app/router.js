import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import ReactGA from 'react-ga';

import Particles from '../../components/particles';
import Header from '../../components/header';
import Footer from '../../components/footer';
import NoMatch from '../../components/not-found';

import routes from './routes';

ReactGA.initialize('UA-43222844-4');
const history = createBrowserHistory();

history.listen(location => {
  ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
});

export default () => (
  <Router history={history}>
    <React.Fragment>
      <Particles />
      <Header />
      <main className="wrapper">
        <Switch>
          {routes.map((route, i) => (
            <Route {...route} key={i} />
          ))}
          <Route component={NoMatch} />
        </Switch>
      </main>
      <Footer />
    </React.Fragment>
  </Router>
);
