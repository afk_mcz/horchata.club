import fetch from 'isomorphic-fetch';
import constants from '../constants';

const apiURL = constants.APIURL;

export const API_RESPONSE = 'API_RESPONSE';
export const API_REQUEST = 'API_REQUEST';

function apiRequest(endPoint) {
  return {
    type: API_REQUEST,
    endPoint,
  };
}

export function apiResponse(json, endPoint) {
  return {
    type: API_RESPONSE,
    endPoint,
    items: json,
    receivedAt: Date.now(),
  };
}

export function apiFetch(endPoint) {
  return dispatch => {
    dispatch(apiRequest(endPoint));
    return fetch(apiURL + endPoint)
      .then(response => response.json())
      .then(json => dispatch(apiResponse(json, endPoint)));
  };
}

function apiShouldFetch(state, endPoint) {
  const apiCall = state.apiCalls[endPoint];
  if (!apiCall) {
    return true;
  }
  if (apiCall.isFetching) {
    return false;
  }
  return false;
}

export function apiFetchIfNeeded(endPoint) {
  return (dispatch, getState) => {
    if (apiShouldFetch(getState(), endPoint)) {
      // Dispatch a thunk from thunk!
      return dispatch(apiFetch(endPoint));
    }
    // Let the calling code know there's nothing to wait for.
    return Promise.resolve();
  };
}

export const FILE_REQUEST = 'FILE_REQUEST ';
function fileRequest(endPoint) {
  return {
    type: FILE_REQUEST,
    endPoint,
  };
}

export const FILE_RESPONSE = 'FILE_RESPONSE';
export function fileResponse(text, endPoint) {
  return {
    type: FILE_RESPONSE,
    endPoint,
    file: text,
    receivedAt: Date.now(),
  };
}

export function fileFetch(endPoint) {
  return dispatch => {
    dispatch(fileRequest(endPoint));
    return fetch(endPoint)
      .then(response => response.text())
      .then(text => dispatch(fileResponse(text, endPoint)));
  };
}

function fileShouldFetch(state, endPoint) {
  const fileCall = state.fileCalls[endPoint];
  if (!fileCall) {
    return true;
  }
  if (fileCall.isFetching) {
    return false;
  }
  return false;
}

export function fileFetchIfNeeded(endPoint) {
  return (dispatch, getState) => {
    if (fileShouldFetch(getState(), endPoint)) {
      return dispatch(fileFetch(endPoint));
    }
    return Promise.resolve();
  };
}

export const SET_NAME_FILTER = 'SET_NAME_FILTER';
export const CLEAR_NAME_FILTER = 'CLEAR_NAME_FILTER';

export function setNameFilter(filter) {
  return {
    type: SET_NAME_FILTER,
    filter,
  };
}

export function clearNameFilter() {
  return {
    type: CLEAR_NAME_FILTER,
  };
}

export const SET_VISIBLE_ITEMS = 'SET_VISIBLE_ITEMS';
export function setVisibleItems(items) {
  const ITEMS = items || [];
  return {
    type: SET_VISIBLE_ITEMS,
    items: ITEMS,
  };
}
