/* eslint camelcase: 0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Grade from '../grade';

import './style.css';

const Row = ({ id, name, image, small_text, grade, address, location }) => {
  const locationArr = location.split(',');
  const googleLink = `https://google.com/maps/?=${locationArr[0]},${
    locationArr[1]
  }`;

  return (
    <article className="row">
      <div className="card">
        <Link to={`/${id}`}>
          <img className="horchata__img" src={image} alt={name} />
        </Link>
        <div className="horchata__body">
          <h3>
            <Link to={`/${id}`}>{name}</Link>
          </h3>
          <Grade grade={grade} />
          <div className="description">
            <p>{small_text}</p>
          </div>
          <div className="address">
            <a href={googleLink} rel="noopener noreferrer" target="_blank">
              {address}
            </a>
          </div>
        </div>
      </div>
    </article>
  );
};

Row.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  small_text: PropTypes.string.isRequired,
  grade: PropTypes.number.isRequired,
  address: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
};

export default Row;
