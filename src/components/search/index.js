import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Search = ({ filter, onChange }) => (
  <form className="search">
    <input
      type="text"
      name="search"
      placeholder="Buscar ..."
      value={filter}
      onChange={onChange}
    />
  </form>
);

Search.propTypes = {
  filter: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Search;
