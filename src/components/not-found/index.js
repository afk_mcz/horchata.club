import React from 'react';
import { withRouter } from 'react-router';
import './style.css';

const NotFound = () => (
  <div className="not-found card rich-text">
    <pre className="404">/404</pre>
    <h1>No se encontro la página que buscas</h1>
    <p>
      Si crees que esto es un error, mandale un correo a{' '}
      <a href="mailto:afk@ellugar.co">afk@ellugar.co</a> e intentará arreglarlo.
    </p>
  </div>
);

export default withRouter(NotFound);
