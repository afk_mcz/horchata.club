import React from 'react';
import Particles from 'react-particles-js';

import config from './config';
import './style.css';

const CinnamonParticles = () => (
  <Particles className="particles" params={config} />
);

export default CinnamonParticles;
