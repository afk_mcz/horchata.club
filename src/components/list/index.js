import React from 'react';
import PropTypes from 'prop-types';
import Row from '../row';
import Loading from '../loading';

import './style.css';

const List = ({ items, isFetching }) => {
  if (isFetching) return <Loading />;
  return (
    <div className="horchata-list">
      {items.map(item => (
        <Row key={item.id} {...item} onClick={() => console.log('clicked')} />
      ))}
    </div>
  );
};

List.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
  isFetching: PropTypes.bool,
};

List.defaultProps = {
  isFetching: false,
};

export default List;
